#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>
#include <limits.h>
#include <dirent.h>
#include <string.h>

#define PORT_NUMBER 8080
#define SERVER_ADDRESS "127.0.0.1"
#define TRUE 1
#define FALSE 0

int upload_count = 0;
int download_count = 0;

struct message_data{
	char *command;
	char *target;
	char *message;
	int socket;
	//int thread_number;
};

void *upload_file(void *ptr){
	//int socket is passed
	int socket_desc;
	char *message;
	char *reply;
	struct message_data *passed_data;
	reply = (char*)malloc(BUFSIZ*sizeof(char));
	passed_data = (struct message_data*)malloc(1*sizeof(struct message_data));
	passed_data = (struct message_data *) ptr;
	socket_desc = passed_data->socket;
	message = passed_data->message;
	if(send(socket_desc, message, strlen(message), 0) < 0){
		puts("Send failed");
	} else{
		printf("Sent message: %s %s\n", passed_data->command, passed_data->target);
		//Succeed
		//Ask for acknowledgement
		if(recv(socket_desc, reply, BUFSIZ, 0) < 0){
			puts("recv failed");
			fprintf(stderr, "Error on receive --> %s", strerror(errno));
		} else{
			//printf("Reply received: %s\n", reply);
		}
		while (strcmp(reply, "upack") != 0){
			send(socket_desc, message, strlen(message), 0);
			recv(socket_desc, reply, BUFSIZ, 0);
		}
		//printf("UPLOAD ACKNOWLEDGED\n");
		//Sending proper
		char *fs_name;
		char *file_buffer;

		fs_name = passed_data->target;
		FILE *fs = fopen(fs_name, "r");
		file_buffer = (char*)calloc(BUFSIZ, sizeof(char));

		/*check niya kung NULL yung file*/
		if(fs == NULL)
        {
            printf("ERROR: File %s not found.\n", fs_name);
            exit(1);
        }
		
		int fs_block_size; //the file buffer block size
		while((fs_block_size = fread(file_buffer, sizeof(char), 512, fs)) > 0) //fread reads the file(with error detecting shits)
        {
            if(send(socket_desc, file_buffer, fs_block_size, 0) < 0) //sends the file buffer to server(with error detecting shits)
            {
                fprintf(stderr, "ERROR: Failed to send file %s. (errno = %d)\n", fs_name, errno);
                break;
            } else{
            	//printf("%s", file_buffer);
            }
        }
		printf("\nFile %s from Client was Sent!\n", fs_name);
		/*SENDING FILE TO SERVER END*/
		fclose(fs);
	}

	close(socket_desc);}

void *download_file(void *ptr){
	//int socket is passed
	int socket_desc;
	char *message;
	char *reply;
	struct message_data *passed_data;
	reply = (char*)malloc(BUFSIZ*sizeof(char));
	passed_data = (struct message_data*)malloc(1*sizeof(struct message_data));
	passed_data = (struct message_data *) ptr;
	socket_desc = passed_data->socket;
	message = passed_data->message;
	if(send(socket_desc, message, strlen(message), 0) < 0){
		fprintf(stderr, "Error sending data for download --> %s\n", strerror(errno));
		//puts("Send failed");
	} else{
		if(recv(socket_desc, reply, BUFSIZ, 0) < 0){
			puts("recv failed -- download_file");
			fprintf(stderr, "Error on receive --> %s", strerror(errno));
		} else{
			while (strcmp(reply, "dlack") != 0){
				send(socket_desc, message, strlen(message), 0);
				recv(socket_desc, reply, BUFSIZ, 0);
			}
			//printf("ACKNOWLEDGED\n");
			//once dl is acknowledged
			char *fr_name, *filename;
			char *reply_buffer;
			int shortened = FALSE;

			filename = passed_data->target;
			//recv(socket_desc, fr_name, BUFSIZ, 0);
			for(int i = strlen(filename) - 1; i >= 0; i--){
				if(filename[i] == '/'){
					fr_name = (char*)calloc(strlen(filename) - i, sizeof(char));
					fr_name = strncpy(fr_name, filename + i + 1, strlen(filename) - i);	
					shortened = TRUE;
					break;
				}
			}

			if(shortened == FALSE){
				fr_name = filename;
			}

			//printf("PASSED FILENAME + %s\n", fr_name);

			reply_buffer = (char*)calloc(BUFSIZ, sizeof(char));
			FILE *fr = fopen(fr_name, "a");
			//printf("FILE ALLOCATED\n");

			//int fr_block_size = 0;
		    int loop_number = 0;

		    printf("File is: %s\n", fr_name);
			if(fr == NULL){
				printf("File %s cannot be opened.\n", fr_name);
			}
			else
			{
				//printf("FILE OPENED!\n");
			    int fr_block_size = 0;
			    while((fr_block_size = recv(socket_desc, reply_buffer, 512, 0)) > 0)
			    {
			    	//printf("Writing...\n");
			        int write_size = fwrite(reply_buffer, sizeof(char), fr_block_size, fr);
			        if(write_size < fr_block_size)
			        {
			            printf("File write failed.\n");
			        }
			        if (fr_block_size == 0 || fr_block_size != 512) 
			        {
			            break;
			        }
			    }
			    if(fr_block_size < 0)
			    {
			        if (errno == EAGAIN)
			        {
			            printf("recv() timed out.\n");
			        }
			        else
			        {
			            fprintf(stderr, "recv() failed due to errno = %d\n", errno);
			        }
			    }
			    printf("Download Complete!\n");
				/*RECIEVING FILE FROM SERVER END*/
			    fclose(fr);
			}
		}
		//printf("Sent message: %s\n", message);
		//Succeed
	}
	close(socket_desc);
}

void *sock_comm(void *ptr){
	int *socket_desc;
	struct sockaddr_in server;
	int *upload_sockets, *download_sockets;
	pthread_t *upload_threads, *download_threads;
	char *message;

	upload_threads = (pthread_t*)malloc(1*sizeof(pthread_t));
	download_threads = (pthread_t*)malloc(1*sizeof(pthread_t));

	upload_sockets = (int*)malloc(1*sizeof(int));
	download_sockets = (int*)malloc(1*sizeof(int));

	//Server details
	server.sin_addr.s_addr = inet_addr(SERVER_ADDRESS);
	//printf("address: %u\n", server.sin_addr.s_addr);
	server.sin_family = AF_INET;
	server.sin_port = htons(PORT_NUMBER);

	socket_desc = (int *) ptr;
	int running = TRUE;
	while(running == TRUE){
		message = (char*)calloc(BUFSIZ, sizeof(char));
		//scanf("%s", message);
		printf("\nEnter command: ");
		fgets(message, BUFSIZ, stdin);	//includes carriage return
		message = strtok(message, "\n");
		// if (send(*socket_desc, message, strlen(message), 0) < 0){	//move sending to different part
		// 	puts("Send failed");
		// 	//return 1;
		// } else{
		// 	//printf("Data sent: %s\n", message);
		// }
		if (strlen(message) > 3){
			//Parse command
			//Get first two letteres (command)
			char *command, *target;

			FILE *target_file;
			command = (char*)malloc(2*sizeof(char));
			target = (char*)malloc((strlen(message) - 3)*sizeof(char));
			strncpy(command, message, 2);
			strncpy(target, message + 3, strlen(message) - 3);
			//printf("Command: %s\nTarget: %s\n", command, target);

			//Open File
			//if
			//target_file = fopen(target, );

			if(strcmp(command, "cd") == 0){
				printf("Move to client directory: %s\n", target); //NO USE ON SERVER! keep on client side!
			}
			else if(strcmp(command, "sd") == 0){
				printf("Move to server directory: %s\n", target);
			}
			else if(strcmp(command, "up") == 0){
				printf("Upload from client: %s\n", target);
				//Connecting to server
				if(upload_count > 0){
					upload_sockets = (int*)realloc(upload_sockets, (upload_count + 1)*sizeof(int));
					upload_threads = (pthread_t*)realloc(upload_threads, (upload_count + 1)*sizeof(pthread_t));
				}
				upload_sockets[upload_count] = socket(AF_INET, SOCK_STREAM, 0);
				if (connect(upload_sockets[upload_count], (struct sockaddr *)&server, sizeof(server)) < 0){
					fprintf(stderr, "Error creating new download socket --> %s\n", strerror(errno));
					puts("connect error");
				} else{
					struct message_data *passed_message;
					passed_message = (struct message_data*)malloc(1*sizeof(struct message_data));
					passed_message->command = command;
					passed_message->target = target;
					passed_message->socket = upload_sockets[upload_count];
					//passed_message->thread_number = upload_count + 1;
					passed_message->message = message;
					upload_file(passed_message);
					//pthread_create(&upload_threads[upload_count], NULL, upload_file, passed_message);
					upload_count++;
					//puts("CONNECTED\n");
				}
			}
			else if(strcmp(command, "dl") == 0){
				printf("Download from server: %s\n", target);
				//Connecting to server
				if(download_count > 0){
					download_sockets = (int*)realloc(download_sockets, (download_count + 1)*sizeof(int));
					download_threads = (pthread_t*)realloc(download_threads, (download_count + 1)*sizeof(pthread_t));
				}
				download_sockets[download_count] = socket(AF_INET, SOCK_STREAM, 0);
				if (connect(download_sockets[download_count], (struct sockaddr *)&server, sizeof(server)) < 0){
					fprintf(stderr, "Error creating new download socket --> %s\n", strerror(errno));
					puts("connect error");
				} else{
					struct message_data *passed_message;
					passed_message = (struct message_data*)malloc(1*sizeof(struct message_data));
					passed_message->command = command;
					passed_message->target = target;
					passed_message->socket = download_sockets[download_count];
					//passed_message->thread_number = download_count + 1;
					passed_message->message = message;
					download_file(passed_message);
					//pthread_create(&download_threads[download_count], NULL, download_file, passed_message);
					download_count++;
					//pthread_join(download_threads[download_count], NULL);
					//puts("CONNECTED\n");
				}
			}
			else if(strcmp(command, "dt") == 0){
				if (send(*socket_desc, message, strlen(message), 0) < 0){	//move sending to different part
					puts("Send failed");
					//return 1;
				} else{
					//printf("Data sent: %s\n", message);
				}
				printf("Delete from server: %s\n", target);
			}
			else if(strcmp(message, "quit") == 0){							//move to end
				if (send(*socket_desc, message, strlen(message), 0) < 0){	//move sending to different part
					puts("Send failed");
					//return 1;
				} else{
					//printf("Data sent: %s\n", message);
				}
				running = FALSE;
			}
			//puts("COMMAND NOT FOUND\n");
		}
		else{
			if(strcmp(message, "ls") == 0){
				//puts("LISTS FILES\n");
				char *cwd;
				cwd = (char*)calloc(PATH_MAX + 1, sizeof(char));
				//printf("Size of cwd: %lu\n", sizeof(cwd));
				if(getcwd(cwd, PATH_MAX + 1) != NULL){
					printf("\nCurrent directory: %s\n", cwd);
				} else{
					fprintf(stderr, "Error on listing files --> %s\n", strerror(errno));
				}
				//List files on client
				DIR *dir;
				struct dirent *ent;
				printf("Files in Client directory:\n==============\n");
				if((dir = opendir(cwd)) != NULL){
					while((ent = readdir(dir)) != NULL){
						printf("%s\n", ent->d_name);
					}
					closedir(dir);
				} else{
					//printf("Error while printing directory files");
					fprintf(stderr, "Error printing directory files --> %s\n", strerror(errno));
				}
				printf("\n");
				//Send request to list files
				if (send(*socket_desc, message, strlen(message), 0) < 0){	//move sending to different part
					puts("Send failed");
					//return 1;
				} else{
					//printf("Data sent: %s\n", message);
				}
				//Receive message
				char *reply;
				reply = (char*)calloc(BUFSIZ, sizeof(char));
				if(recv(*socket_desc, reply, BUFSIZ, 0) < 0){
					puts("recv failed");
					fprintf(stderr, "Error on receiving list of files --> %s", strerror(errno));
					exit(EXIT_FAILURE);
					running = FALSE;
				} else{
					//printf("Reply received: %s\n", reply);
					//LOOP UNTIL "endls" received
					printf("Server Files:\n=============\n");
					while(strcmp(reply, "endls") != 0){
						recv(*socket_desc, reply, BUFSIZ, 0);
						if(strcmp(reply, "endls") != 0){
							printf("%s\n", reply);
						}
					}
				}
			}
			else{
				puts("COMMAND NOT FOUND\n");
			}
			//puts("COMMAND NOT FOUND\n");
		}
		free(message);	
	}
}

int main(int argc, char *argv[]){
	int socket_desc;
	struct sockaddr_in server;
	char *server_reply;
	pthread_t *main_thread;
	int mthread_ret;

	//Create socket
	socket_desc = socket(AF_INET, SOCK_STREAM, 0);

	if (socket_desc == -1){
		printf("Could not create socket");
	}

	//Server details
	server.sin_addr.s_addr = inet_addr(SERVER_ADDRESS);
	//printf("address: %u\n", server.sin_addr.s_addr);
	server.sin_family = AF_INET;
	server.sin_port = htons(PORT_NUMBER);

	//Connecting to server
	if (connect(socket_desc, (struct sockaddr *)&server, sizeof(server)) < 0){
		puts("connect error");
		return 1;
	} else{
		puts("CONNECTED\n");
	}

	main_thread = (pthread_t*)malloc(1*sizeof(pthread_t));
	mthread_ret = pthread_create(main_thread, NULL, sock_comm, &socket_desc);

	pthread_join(*main_thread, NULL);
	close(socket_desc);

	return 0;
}

/*
References:
http://stackoverflow.com/questions/612097/how-can-i-get-the-list-of-files-in-a-directory-using-c-or-c
http://www.qnx.com/developers/docs/660/index.jsp?topic=%2Fcom.qnx.doc.neutrino.lib_ref%2Ftopic%2Fg%2Fgetcwd.html
http://stackoverflow.com/questions/298510/how-to-get-the-current-directory-in-a-c-program

Socket Programming:
http://www.binarytides.com/socket-programming-c-linux-tutorial/

Sending and Receiving Files:
http://stackoverflow.com/questions/11952898/c-send-and-receive-file

Socket documentation:
http://pubs.opengroup.org/onlinepubs/7908799/xns/syssocket.h.html

Threading:
http://www.yolinux.com/TUTORIALS/LinuxTutorialPosixThreads.html
*/